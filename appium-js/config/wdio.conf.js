exports.config = {
    // wdio configuration
    // see details: https://webdriver.io/docs/options.html
    runner: 'local',

    // specify test files
    specs: [
        './features/**/*.feature'
    ],
    // patterns to exclude
    exclude: [
        // 'path/to/excluded/files'
    ],

    // capabilities
    maxInstances: 10,

    capabilities: [{
        app: "./app/app-debug.apk",
        appActivity: "com.appium.helloworld.MainActivity",
        appPackage: "com.appium.helloworld",
        avd: "Pixel_3_API_28",
        fullReset: false, // false: don't uninstall and re-install app, true: do uninstall and re-install app
        noReset: true, // true: don't clear cached app data, false: do clear cached app data
        platformName: "Android",
        platformVersion: "9",
    }],

    // test configurations
    logLevel: 'info', // level of logging verbosity: trace | debug | info | warn | error | silent
    bail: 0, // bail (default is 0 - don't bail, run all tests)
    waitforTimeout: 10000, // default timeout for all waitfor* commands
    connectionRetryTimeout: 120000, // default timeout in milliseconds for request if browser driver or grid doesn't send response
    connectionRetryCount: 3, // default request retries count

    // test runner services
    services: ['appium'],

    // appium service config
    // see details: https://webdriver.io/docs/appium-service.html
    appium: {
        command: 'appium',
        waitStartTime: 6000,
        logFileName: 'appium.log',
        args: {
            address: '0.0.0.0',
            port: 4723,
            path: '/wd/hub',
            commandTimeout: '7200',
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    // framework you want to run your specs with
    // see also: https://webdriver.io/docs/frameworks.html
    framework: 'cucumber',

    // the number of times to retry the entire specfile when it fails as a whole
    specFileRetries: 1,

    // whether or not retried specfiles should be retried immediately or deferred to the end of the queue
    specFileRetriesDeferred: false,

    // test reporter for stdout
    // see details: https://webdriver.io/docs/allure-reporter.html
    reporters: [['allure', {
        outputDir: 'allure-results',
        useCucumberStepReporter: true
    }]],

    // specify the location of your step definitions
    cucumberOpts: {
        require: ['./dist/features/step-definitions/*.js'],        // <string[]> (file/dir) require files before executing features
        backtrace: true,    // <boolean> show full backtrace for errors
        requireModule: [],  // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        dryRun: false,      // <boolean> invoke formatters without executing steps
        failFast: false,    // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        snippets: true,     // <boolean> hide step definition snippets for pending steps
        source: true,       // <boolean> hide source uris
        profile: [],        // <string[]> (name) specify the profile to use
        strict: false,      // <boolean> fail if there are any undefined or pending steps
        tagExpression: '',  // <string> (expression) only execute the features or scenarios with tags matching the expression
        timeout: 60000,     // <number> timeout for step definitions
        ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings.
    },

    // hooks
    /**
     * gets executed once before all workers get launched
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    onPrepare: function (config, capabilities) {
        console.log("appium.tests.started");
    },

    /**
     * runs after a cucumber scenario
     */
    afterScenario: function (uri, feature, scenario, result, sourceLocation) {
        console.log("browser.saveScreenshot");
    },

    /**
     * gets executed after all workers got shut down and the process is about to exit
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {<Object>} results object containing test results
     */
    onComplete: function (exitCode, config, capabilities, results) {
        console.log("appium.tests.finished")
        // stop appium service
        this.stopAppiumService('windows');
    },

    stopAppiumService: function (os) {
        if (os == 'windows') {
            var theJobType = '/f /im node.exe';
            var exec = require('child_process').exec;
            var child = exec('taskkill ' + theJobType, function (error, stdout, stderr) {
                if (error != null) {
                    console.log(stderr);
                    // error handling & exit
                }
                // normal
            });
        }
    }
}
