import { Element } from "webdriverio";

class HelloWorldPageObject {
    public get helloWorldTextView(): Promise<Element> { return $("id=com.appium.helloworld:id/id_hello_world"); };
    public get todayIsTextView(): Promise<Element> { return $("id=com.appium.helloworld:id/id_today_is"); };
    public get greetingMessageTextView(): Promise<Element> { return $("id=com.appium.helloworld:id/id_greeting_message"); };
}

export const HelloWorldPage = new HelloWorldPageObject();

