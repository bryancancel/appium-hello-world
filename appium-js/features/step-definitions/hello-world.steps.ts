import assert from 'assert';
import { Given, When, Then } from 'cucumber';
import { HelloWorldPage } from '../../pages/hello-world.page';

//#region Launch app and Hello World appears
Given('My app is launched', async function () {
    const appId: string = await driver.getCurrentPackage();
    const appState: number = await driver.queryAppState(appId);

    /*
    0: The current application state cannot be determined/is unknown
    1: The application is not running
    2: The application is running in the background and is suspended
    3: The application is running in the background and is not suspended
    4: The application is running in the foreground
    see details: http://appium.io/docs/en/writing-running-appium/ios/ios-xctest-mobile-apps-management/index.html#mobile-queryappstate
    */
    const RUNNING_IN_FOREGROUND = 4

    assert.equal(appState, RUNNING_IN_FOREGROUND);
});

When('I look at it for the first time', async function () {
    const element = await HelloWorldPage.helloWorldTextView;
    const value = await element.isDisplayed();
    assert.equal(value, true);
});

Then('I should be greeted with a {string}', async function (greeting) {
    const element = await HelloWorldPage.helloWorldTextView;
    const value = await element.getText();
    assert.equal(value, greeting);
});
//#endregion

//#region Today is or is not Friday
Given('Today is {string}', async function (givenDay) {
    const element = await HelloWorldPage.todayIsTextView;
    await element.setValue(givenDay);
    /*
    Key code constant: Enter key.
    Constant Value: 66 (0x00000042)
    see details: https://developer.android.com/reference/android/view/KeyEvent.html#KEYCODE_ENTER
    */
    const KEYCODE_ENTER = 66;
    driver.pressKeyCode(KEYCODE_ENTER);
});

When('I ask whether it\'s Friday yet', async function () {
    const element = await HelloWorldPage.greetingMessageTextView;
    const value = await element.getText();
    this.actualAnswer = value;
});

Then('I should be told {string}', function (expectedAnswer) {
    assert.equal(this.actualAnswer, expectedAnswer);
});
//#endregion