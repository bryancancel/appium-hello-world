Feature: Hello World
    Everybody wants to see Hello World

    Scenario Outline: Launch app and Hello World appears
        Given My app is launched
        When I look at it for the first time
        Then I should be greeted with a "<message>"

        Examples:
            | message      |
            | Hello World! |

    Scenario Outline: Today is or is not Friday
        Given Today is "<day>"
        When I ask whether it's Friday yet
        Then I should be told "<answer>"

        Examples:
            | day       | answer |
            | Monday    | Nope   |
            | Tuesday   | Nope   |
            | Wednesday | Nope   |
            | Thursday  | Nope   |
            | Friday    | TGIF   |
            | Saturday  | Nope   |
            | Sunday    | Nope   |