
var common = [
    '--dry-run',
    `--format-options '{`+
      '"colorsEnabled": true, ' +
      '"snippetInterface": "asyncawait"' +
    `}'`
  ].join(' ')

module.exports = {
    default: common
}