# Appium Hello World

## About
A simple hello world repo to help you get started with Appium. You can click on the thumbnail below to download a demo video.

[![IMAGE](media/appium-hello-world.png)](https://bitbucket.org/bryancancel/appium-hello-world/raw/96fec8ba47e2faf42bf588ea7f828d8f89b299b6/media/appium-hello-world.mp4)

### Built With

* [VS Code](https://code.visualstudio.com/) -- used to build appium scripts to test a sample android app
* [Android Studio](https://developer.android.com/studio) -- used to build a sample android app

## Getting Started

You will need to have the following pre-requisites satisfied:

- Node
- Appium
- Android

Then, all you need to do is run the following commands:

```
cd \appium-js
npm install
npm test
npm run report
```

## Prerequisites

To get a local copy up and running you will need to first install the following pre-requisite software:

### Node

1. Check if node.js is installed on your system    
```
node -v
npm -v
```
2. Download node.js installer    https://nodejs.org/en/download/
3. Run the installer & install node.js & npm
4. Check if node.js & npm are installed 
```
node -v 
npm -v
```
### Appium

1. Install appium with node.js
```
npm init -y
npm install -g appium
```
2. Check if appium is installed
```
appium -v
```
3. Start appium (ctr+z to terminate)
```
appium
```
### Appium Desktop Client

1. Download appium desktop client
	1. https://github.com/appium/appium-desktop/releases/latest
2. Install appium desktop client
3. Start appium through appium desktop client
4. Check appium installation & dependencies
	1. Install appium-doctor https://github.com/appium/appium-doctor
```
npm install appium-doctor -g
appium-doctor -h
appium-doctor --android (ANDROID_HOME will be created later after installing android studio)
```
### Android Studio and Emulator

1. Download and install android studio
	1. https://developer.android.com/studio
	1. Note: If you're using hyper-v, you'll also need to configure [WHPX](https://docs.microsoft.com/en-us/xamarin/android/get-started/installation/android-emulator/hardware-acceleration?pivots=windows)
2. Create a Hello World App (can also open an existing project from /android folder)
	1. Checkout [How to Install Android IDE and SDK and Get Started with Android Programming](https://www3.ntu.edu.sg/home/ehchua/programming/android/Android_HowTo.html) for additional information to get you up and running with your first hello world app
3. Run app on emulator
	1. In AVD manager create a device that targets Android 6.0 or Higher e.g. Pixel 3 28
	1. Given a brand new emulator, when starting up for the first time, then let the emulator finish all of its updates before running the app, which will deploy the apk onto the emulator, otherwise you will get "connect to device" type errors when running from Android Studio
	1. Troubleshooting: 
		1. If you have trouble connecting to device from Android Studio, you can delete and create a new device
4. Configure the following environment variables
	1. `ANDROID_HOME = C:\Users\%USERNAME%\AppData\Local\Android\Sdk` 
	2. `%PATH% = %PATH%;%ANDROID_HOME%\platform-tools`
	3. `%PATH% = %PATH%;%ANDROID_HOME%\tools`
5. Build APK
	1. `Build > Build APK`
	1. Confirm output to `.\android\app\build\outputs\apk\debug`



## Usage

The following are some test development tips captured during the development of the test automation scripts:

**Create snippets for undefined cucumber steps**
```
npm run cucumber
``` 

**Ensure devices are visible to the appium host**
```
adb devices
```

**Get list of avds**
```
emulator -list-avds
```

**Run emulator from command line**
```
appium --avd "Pixel_3_API_28"
```

**Identify a mobile element selector**

You have lots of different options for identifying app selectors!

- [appium desktop inspector video 1](https://www.youtube.com/watch?v=1ot8cZoUk6o)
- [appium desktop inspector video 2](https://www.youtube.com/watch?v=P2lM4NY4CTU)
- launch `%ANDROID_HOME%\tools\bin\uiautomationviewer.bat`
- install `APK Info` from play store find your app and identify activities
- obtain device information needed for desired capabilities from device itself (browse to settings > about)

## Roadmap

See the [open issues](https://bitbucket.org/bryancancel/appium-hello-world/jira?statuses=new&statuses=indeterminate&sort=-updated&page=1) for a list of proposed features (and known issues)

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

[ISC License (ISC) | Open Source Initiative](https://opensource.org/licenses/ISC)

## Contact

[Bryan Cancel](bryan.cancel@slalom.com)

## Acknowledgements

* http://appium.io/docs/en/about-appium/getting-started/?lang=en -- initial getting started guide at appium
* https://github.com/appium/appium/tree/master/sample-code/javascript-webdriverio/test/basic -- sample code from appium
* https://www.qsstechnosoft.com/how-to-setup-appium-with-cucumber-and-node-js -- javascript example
* https://github.com/igniteram/appium-webdriverio-typescript/ -- typescript example