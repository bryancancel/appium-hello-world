package com.appium.helloworld

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // wire up what happens when user hits enter in today is text box
        id_today_is.setOnKeyListener(View.OnKeyListener { view, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                var todayIsEditText = view as EditText
                id_greeting_message.text = this.generateAnswerString( todayIsEditText.text.toString() )
            }
            false
        })
    }

    private fun generateAnswerString(dayOfTheWeek: String): String {
        return if (dayOfTheWeek.trim().toLowerCase() != "friday") {
            "Nope"
        } else {
            "TGIF"
        }
    }
}
